package com.geo.posts

import com.geo.posts.post.data.model.entity.PostEntity
import com.geo.posts.post.data.repository.PostRepositoryImpl
import com.geo.posts.post.data.repository.datasource.PostApiDataSource
import com.geo.posts.post.data.repository.datasource.PostLocalDataSource
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Flowable
import org.junit.Before
import org.junit.Test

class RepositoryTest {

    lateinit var postRepositoryImpl: PostRepositoryImpl
    var mockApiDataSource: PostApiDataSource = mock()
    var mockLocalDataSource: PostLocalDataSource = mock()

    @Before
    fun setUp() {
        postRepositoryImpl = PostRepositoryImpl(mockApiDataSource, mockLocalDataSource)
    }

    @Test
    fun `test repository should give post list when isReload is false fort both repositories`() {
        val localposts = listOf(
            PostEntity( 1, 1, "Local Title 1", "Local body 1", false, false),
            PostEntity( 2, 2, "Local Title 2", "Local body 2", false, false)
        )

        val apiposts = listOf(
            PostEntity( 1, 3, "Api Title 1", "Api body 1", false, false),
            PostEntity( 2, 4, "Api Title 2", "Api body 2", false, false)
        )

        val obsLocal = Flowable.just(localposts)
        val obsApi = Flowable.just(apiposts)

        whenever(mockLocalDataSource.getAllPosts()).thenReturn(obsLocal)
        whenever(mockApiDataSource.getAllPosts()).thenReturn(obsApi)

        val result = postRepositoryImpl.getPostList(false)

        result
            .firstElement()
            .test()
            .assertValue { it.size == 2 }
            .assertValue {
                it.first().title == localposts.first().title
            }
            .assertValue { it.first().id == localposts.first().id }

        result
            .lastElement()
            .test()
            .assertValue { it.size == 2 }
            .assertValue {
                it.first().title == apiposts.first().title
            }
            .assertValue { it.first().id == apiposts.first().id }
    }

    @Test
    fun `test repository should give post list when isReload is true and only one dataset`() {

        val apiposts = listOf(
            PostEntity( 1, 3, "Api Title 1", "Api body 1", false, false),
            PostEntity( 2, 4, "Api Title 2", "Api body 2", false, false)
        )

        val obsApi = Flowable.just(apiposts)

        whenever(mockApiDataSource.getAllPosts()).thenReturn(obsApi)

        val result = postRepositoryImpl.getPostList(true)

        result
            .test()
            .assertValue { it.size == 2 }
            .assertValue {
                it.first().title == apiposts.first().title
            }
            .assertValue { it.first().id == apiposts.first().id }

    }




}