package com.geo.posts

import com.geo.posts.post.data.mapper.PostEntityMapper
import com.geo.posts.post.data.model.response.Post
import org.junit.Assert
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class MapperTest {
    private lateinit var postEntityMapper: PostEntityMapper

    @Before
    fun setUp() {
        postEntityMapper = PostEntityMapper()
    }

    @Test
    fun `Transform Post into PostEntity`() {
        val post = Post(1, 1, "Local Title 1", "Local body 1")
        val postEntity = postEntityMapper.transform(post)
        assertEquals(postEntity.userId, post.userId)
        assertEquals(postEntity.title, post.title)
        assertEquals(postEntity.id, post.id)
        assertEquals(postEntity.body, post.body)
    }

    @Test
    fun `Transform Post list into Post entity list`() {
        val postList = listOf(
            Post(1, 1, "Local Title 1", "Local body 1"),
            Post(1, 2, "Local Title 2", "Local body 2")
        )

        val postEntities = postEntityMapper.transformList(postList)
        assertEquals(postList.size, postEntities.size)
        assertEquals(postList.first().id, postEntities.first().id)
        Assert.assertNotNull(postEntities.first().title)
        Assert.assertNotNull(postEntities.first().userId)
    }
}