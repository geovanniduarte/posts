package com.geo.posts

import com.geo.posts.base.exception.HttpCallFailureException
import com.geo.posts.post.data.model.entity.PostEntity
import com.geo.posts.post.data.repository.PostRepositoryImpl
import com.geo.posts.post.data.repository.datasource.PostApiDataSource
import com.geo.posts.post.data.repository.datasource.PostLocalDataSource
import com.geo.posts.post.usecase.GetAllPostList
import com.nhaarman.mockito_kotlin.*
import io.reactivex.Flowable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.TestScheduler
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import retrofit2.HttpException

class UseCasesTest {

    lateinit var postRepositoryImpl: PostRepositoryImpl
    lateinit var getPostList: GetAllPostList
    lateinit var testScheduler: TestScheduler
    lateinit var compositeDisposable: CompositeDisposable
    var mockApiDataSource: PostApiDataSource = mock()
    var mockLocalDataSource: PostLocalDataSource = mock()

    @Before
    fun setUp() {
        testScheduler = TestScheduler()
        compositeDisposable = CompositeDisposable()
        postRepositoryImpl = mock()
        getPostList = GetAllPostList(postRepositoryImpl, testScheduler, TestScheduler())
    }

    @Test
    fun `when get post list use case executed with isReload true then same list by repository should be acquared`() {

        val posts = listOf(
            PostEntity( 1, 1, "Local Title 1", "Local body 1", false, false)
        )
        val obs = Flowable.just(posts)
        whenever(postRepositoryImpl.getPostList(any())).thenReturn(obs)


        getPostList.buildCase(false)
            .test()
            .assertValue {
                it.first().title == posts.first().title }
            .assertValue { it.first().id == posts.first().id }

    }

    @Test
    fun `when is reload is false and local datasource emits 0 items, api data should be acquired`() {

        postRepositoryImpl = PostRepositoryImpl(mockApiDataSource, mockLocalDataSource)
        getPostList = GetAllPostList(postRepositoryImpl, mock(), mock())

        val apiposts = listOf(
            PostEntity( 1, 3, "Api Title 1", "Api body 1", false, false),
            PostEntity( 2, 4, "Api Title 2", "Api body 2", false, false)
        )

        val apiObs = Flowable.just(apiposts)
        val localObs = Flowable.just(emptyList<PostEntity>())

        whenever(mockApiDataSource.getAllPosts()).thenReturn(apiObs)
        whenever(mockLocalDataSource.getAllPosts()).thenReturn(localObs)

        getPostList.buildCase(false)
            .test()
            .assertValue {
                it.first().title == apiposts.first().title }
            .assertValue { it.first().id == apiposts.first().id }
    }

    @Test
    fun `when is reload is false and local datasource emits n items, local data should be acquired`() {

        postRepositoryImpl = PostRepositoryImpl(mockApiDataSource, mockLocalDataSource)
        getPostList = GetAllPostList(postRepositoryImpl, mock(), mock())

        val localposts = listOf(
            PostEntity( 1, 1, "Local Title 1", "Local body 1", false, false),
            PostEntity( 2, 2, "Local Title 2", "Local body 2", false, false)
        )

        val apiposts = listOf(
            PostEntity( 1, 3, "Api Title 1", "Api body 1", false, false),
            PostEntity( 2, 4, "Api Title 2", "Api body 2", false, false)
        )

        val apiObs = Flowable.just(apiposts)
        val localObs = Flowable.just(localposts)

        whenever(mockApiDataSource.getAllPosts()).thenReturn(apiObs)
        whenever(mockLocalDataSource.getAllPosts()).thenReturn(localObs)

        getPostList.buildCase(false)
            .test()
            .assertValue {
                it.first().title == localposts.first().title }
            .assertValue { it.first().id == localposts.first().id }
    }

    @Test
    fun `when error is throwed error type should be the expected`() {

        val exception : HttpException = HttpException(mock())
        Mockito.`when`(postRepositoryImpl.getPostList(any())).thenReturn(Flowable.error(exception))

        getPostList.buildCase(false)
            .test()
            .assertError{
                it is HttpCallFailureException
            }
    }
}