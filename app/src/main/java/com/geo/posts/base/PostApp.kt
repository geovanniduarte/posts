package com.geo.posts.base

import android.app.Application
import com.geo.posts.post.di.compoment.ApplicationComponent
import com.geo.posts.post.di.compoment.DaggerApplicationComponent
import com.geo.posts.post.di.module.ApplicationModule

private var mApplicationComponent: ApplicationComponent? = null

class PostApp : Application() {

    override fun onCreate() {
        super.onCreate()
        mApplicationComponent = DaggerApplicationComponent.builder()
            .applicationModule(ApplicationModule(this))
            .build()

    }

    fun getComponent() = mApplicationComponent

}