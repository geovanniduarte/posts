package com.geo.posts.base

import android.os.Build
import android.view.View
import androidx.fragment.app.FragmentManager
import androidx.transition.Fade
import com.geo.posts.R
import com.geo.posts.base.Constants.Companion.VIEW_DETAIL_TRANSITION_NAME
import com.geo.posts.post.data.model.entity.PostEntity
import com.geo.posts.post.ui.detail.DetailsTransition
import com.geo.posts.post.ui.detail.PostDetailFragment
import com.geo.posts.post.ui.list.PostListFragment
import com.geo.posts.post.ui.list.tabs.TabsFragment

class Navigator {
    fun navigatesToDetail(supportFragmentManager: FragmentManager, postEntity: PostEntity, view: View) {
        val details: PostDetailFragment = PostDetailFragment.newInstance(postEntity)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            details.sharedElementEnterTransition = DetailsTransition()
            details.enterTransition = Fade()
            details.exitTransition = Fade()
            details.sharedElementReturnTransition = DetailsTransition()
        }

        supportFragmentManager
            .beginTransaction()
            .addSharedElement(view, VIEW_DETAIL_TRANSITION_NAME)
            .replace(R.id.root_fragment, details)
            .addToBackStack(null)
            .commit()
    }

    fun installAllPostsList(supportFragmentManager: FragmentManager, isFavorite: Boolean) {
        if (supportFragmentManager.findFragmentById(R.id.root_fragment) == null) {
            val tabsFragment = TabsFragment.newInstance()
            supportFragmentManager
                .beginTransaction()
                .add(R.id.root_fragment, tabsFragment)
                .addToBackStack(null)
                .commit()
        }
    }
}