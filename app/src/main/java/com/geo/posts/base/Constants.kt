package com.geo.posts.base

class Constants {
    companion object {
        const val POST_DATABASE_NAME = "post.db"
        const val VIEW_DETAIL_TRANSITION_NAME = "sharedView"
    }
}