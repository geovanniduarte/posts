package com.geo.posts.base.vm

interface MVVMViewModel {

    fun handleApiError(error: Throwable?)

    fun setUserAsLoggedOut()
}