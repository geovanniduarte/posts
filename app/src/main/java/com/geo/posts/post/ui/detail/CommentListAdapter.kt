package com.geo.comments.comment.ui.detail

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.geo.posts.R
import com.geo.posts.post.data.model.entity.CommentEntity
import kotlinx.android.synthetic.main.layout_list_comment.view.*

class CommentListAdapter(): RecyclerView.Adapter<CommentListAdapter.CommentsViewHolder>() {

    private lateinit var context: Context
    private var data: MutableList<CommentEntity> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CommentsViewHolder {
        context = parent.context
        return CommentsViewHolder(
            LayoutInflater.from(context)
                .inflate(R.layout.layout_list_comment, parent, false))
    }

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: CommentsViewHolder, position: Int) = holder.bind(data[position])

    fun swapData(data: List<CommentEntity>) {
        this.data.clear()
        this.data.addAll(data)
        notifyDataSetChanged()
    }

    inner class CommentsViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        fun bind(comment: CommentEntity) = with(itemView) {
            txv_comment.text = comment.body
        }
    }
}