package com.geo.posts.post.ui.list

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.geo.posts.R
import com.geo.posts.base.Navigator
import com.geo.posts.base.ui.BaseActivity
import com.geo.posts.post.ui.PostListViewModel
import kotlinx.android.synthetic.main.activity_post_list.*
import javax.inject.Inject

class ListActivity : BaseActivity() {
    

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var navigator: Navigator

    lateinit var viewModel: PostListViewModel

    private var favorite: Boolean = false

    override fun onSaveInstanceState(outState: Bundle) {
        outState?.run {
            putBoolean(FAVORITE, favorite)
        }
        super.onSaveInstanceState(outState)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        savedInstanceState?.run {
            favorite = getBoolean(FAVORITE)
        }
        super.onRestoreInstanceState(savedInstanceState)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        inject()
        setContentView(R.layout.activity_post_list)
        setSupportActionBar(toolbar)
        setUpViewModel()
        setUpView()

        navigator.installAllPostsList(supportFragmentManager, favorite)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean = when(item.itemId) {
        android.R.id.home -> {
            supportFragmentManager.popBackStack()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    private fun inject() {
        mUIComponent?.inject(this)
    }

    private fun setUpViewModel() {
        viewModel = ViewModelProvider(this, viewModelFactory).get(PostListViewModel::class.java)
        bindEvents()
    }

    private fun setUpView() {
        delete_button.setOnClickListener {
            showDeleteDialog()
        }
    }

    private fun showDeleteDialog() {
        val dialog = AlertDialog.Builder(this)
            .setMessage(getString(R.string.message_dialog_you_sure_delete))
            .setTitle(getString(R.string.message_delete_title))
            .setPositiveButton(android.R.string.ok) { dialog, _ ->
                dialog.dismiss()
                viewModel.deleteAllPost()
            }
            .setNegativeButton(android.R.string.cancel) { dialog, _ ->
                dialog.dismiss()
            }
            .create()
        dialog?.show()
    }

    private fun bindEvents() {
        viewModel.isLoadingState.observe(this, Observer { isLoading ->
            if (isLoading) showLoading() else hideLoading()
        })

        viewModel.showHomeButtonState.observe(this, Observer { show ->
            supportActionBar?.setDisplayHomeAsUpEnabled(show)
        })
    }

    override fun showLoading() {
        //super.showLoading()
        list_progress_bar.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        //super.hideLoading()
        list_progress_bar.visibility = View.GONE
    }

    companion object {
        const val FAVORITE: String = "favorite_key"
    }

}
