package com.geo.posts.post.data.repository.datasource

import com.geo.posts.post.data.model.entity.PostEntity
import io.reactivex.Completable

interface LocalDataSource {
    fun savePosts(users: List<PostEntity>)
    fun deletePosts(posts: List<PostEntity>)
    fun deleteAllPosts(): Completable
}