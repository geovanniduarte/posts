package com.geo.posts.post.data.mapper

import com.geo.posts.base.mapper.Mapper
import com.geo.posts.post.data.model.entity.PostEntity
import com.geo.posts.post.data.model.response.Post

class PostEntityMapper: Mapper<Post, PostEntity> {
    override fun transform(input: Post): PostEntity = PostEntity(input.userId, input.id, input.title, input.body)

    override fun transformList(inputList: List<Post>) = inputList.map {
        transform(it)
    }
}