package com.geo.posts.post.usecase

import com.geo.posts.post.data.model.entity.PostEntity
import com.geo.posts.post.data.repository.PostRepositoryImpl
import javax.inject.Inject

class UpdatePostTouched  @Inject constructor(private val postRepositoryImpl: PostRepositoryImpl) {
    fun execute(postEntity: PostEntity) {
        postEntity.touched = true
        postRepositoryImpl.saveUpdatePost(postEntity)
    }
}