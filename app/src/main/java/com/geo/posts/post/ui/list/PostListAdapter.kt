package com.geo.posts.post.ui.list

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.ViewCompat
import androidx.recyclerview.widget.RecyclerView
import com.geo.posts.R
import com.geo.posts.post.data.model.entity.PostEntity
import kotlinx.android.synthetic.main.layout_list_post.view.*

typealias Click = (PostEntity, View) -> Unit
typealias Remove = (PostEntity) -> Unit

class PostListAdapter(val onClickListener: Click, val onRemoveListener: Remove): RecyclerView.Adapter<PostListAdapter.PostsViewHolder>() {

    lateinit var context: Context
    private var data: MutableList<PostEntity> = mutableListOf()
    var recentlyDeletedItem: PostEntity? = null
    var recentlyDeletedItemPosition: Int = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostsViewHolder {
        context = parent.context
        return PostsViewHolder(
            LayoutInflater.from(context)
                .inflate(R.layout.layout_list_post, parent, false))
    }

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: PostsViewHolder, position: Int) = holder.bind(data[position])

    fun clear() {
        this.data.clear()
        notifyDataSetChanged()
    }

    fun swapData(data: List<PostEntity>) {
        this.data.clear()
        this.data.addAll(data)
        notifyDataSetChanged()
    }

    fun deleteLocal(postEntity: PostEntity) {
        val pos = this.data.indexOf(postEntity)
        val last = this.data.size - 1
        if (pos in 0..last) {
            this.data.removeAt(pos)
            notifyItemRemoved(pos)
        }
    }

    fun deleteItem(position: Int) {
        recentlyDeletedItem = this.data[position]
        recentlyDeletedItemPosition = position
        recentlyDeletedItem?.let {
            onRemoveListener(it)
        }
        //showUndoSnackbar()
    }

    inner class PostsViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        fun bind(post: PostEntity) = with(itemView) {

            txv_title.text = post.title
            txv_body.text = post.body

            if (post.id <= 20 && !post.touched) {
                image_dot_post.setImageResource(R.drawable.ic_blue_dot)
            } else {
                image_dot_post.setImageBitmap(null)
            }

            if (post.favorite) {
                image_favorite.setImageResource(R.drawable.ic_star)
            } else {
                image_favorite.setImageResource(R.drawable.ic_empty_star)
            }

            setOnClickListener { onClickListener(post, txv_body) }
            ViewCompat.setTransitionName(txv_body, "${post.id}")
        }
    }
}