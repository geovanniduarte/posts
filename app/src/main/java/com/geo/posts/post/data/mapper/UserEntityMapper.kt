package com.geo.posts.post.data.mapper

import com.geo.posts.base.mapper.Mapper
import com.geo.posts.post.data.model.entity.UserEntity
import com.geo.posts.post.data.model.response.User

class UserEntityMapper: Mapper<User, UserEntity> {
    override fun transform(input: User): UserEntity =
        UserEntity(input.id, input.name, input.email, input.name, input.website)

    override fun transformList(inputList: List<User>): List<UserEntity> =
        inputList.map { transform(it) }
}