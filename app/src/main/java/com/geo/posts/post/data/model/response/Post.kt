package com.geo.posts.post.data.model.response

class Post(
    val userId: Long,
    val id: Long,
    val title: String,
    val body: String
)