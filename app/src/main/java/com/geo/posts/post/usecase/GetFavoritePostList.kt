package com.geo.posts.post.usecase

import com.geo.posts.base.rx.mapNetworkErrors
import com.geo.posts.post.data.model.entity.PostEntity
import com.geo.posts.post.data.repository.PostRepositoryImpl
import io.reactivex.Flowable
import io.reactivex.Scheduler
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import javax.inject.Inject
import javax.inject.Named

class GetFavoritePostList @Inject constructor(private val postRepositoryImpl: PostRepositoryImpl,
                                              @Named("ioScheduler") private val ioScheduler: Scheduler,
                                              @Named("mainScheduler") private val mainScheduler: Scheduler
) {

    fun buildCase(isReload: Boolean): Flowable<List<PostEntity>> =
        postRepositoryImpl.getPostList(isReload)
            .filter { it.isNotEmpty() }
            .firstElement()
            .map {
                it.filter { post -> post.favorite }
            }
            .toFlowable()
            .mapNetworkErrors()

    fun execute(isReload: Boolean, compositeDisposable: CompositeDisposable, onSuccess: (value: List<PostEntity>) -> Unit, onError: (t: Throwable) -> Unit = {}) {
        buildCase(isReload)
            .subscribeOn(ioScheduler)
            .observeOn(mainScheduler)
            .subscribeBy(onNext = onSuccess, onError = onError)
            .addTo(compositeDisposable)
    }
}