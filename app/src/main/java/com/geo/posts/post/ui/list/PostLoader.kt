package com.geo.posts.post.ui.list

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import com.geo.posts.post.data.model.entity.PostEntity
import com.geo.posts.post.ui.PostListViewModel

interface PostLoader {
    fun loadPosts(isReload: Boolean)
    fun observePosts(owner: LifecycleOwner, callback: (List<PostEntity>) -> Unit)
}

class FavoritePostLoader(val viewModel: PostListViewModel):
    PostLoader {
    override fun loadPosts(isReload: Boolean) {
        viewModel.loadFavPosts(isReload)
    }

    override fun observePosts(owner: LifecycleOwner, callback: (List<PostEntity>) -> Unit) {
        viewModel.postFavListState.observe(owner, Observer {
            callback(it)
        })
    }
}

class AllPostLoader(val viewModel: PostListViewModel):
    PostLoader {
    override fun loadPosts(isReload: Boolean) {
        viewModel.loadAllPosts(isReload)
    }

    override fun observePosts(owner: LifecycleOwner, callback: (List<PostEntity>) -> Unit) {
        viewModel.postAllListState.observe(owner, Observer {
            callback(it)
        })
    }
}