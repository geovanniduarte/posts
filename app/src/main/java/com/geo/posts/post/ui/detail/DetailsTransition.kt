package com.geo.posts.post.ui.detail

import androidx.transition.ChangeBounds
import androidx.transition.ChangeImageTransform
import androidx.transition.ChangeTransform
import androidx.transition.TransitionSet

public class DetailsTransition: TransitionSet {
    constructor() {
        ordering = ORDERING_TOGETHER
        addTransition( ChangeBounds())
            .addTransition( ChangeTransform())
            .addTransition( ChangeImageTransform())
    }
}