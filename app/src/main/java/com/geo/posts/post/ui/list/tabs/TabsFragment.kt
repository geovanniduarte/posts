package com.geo.posts.post.ui.list.tabs

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment

import com.geo.posts.R
import com.geo.posts.post.ui.list.PostListFragment
import kotlinx.android.synthetic.main.fragment_tabs.*


class TabsFragment : Fragment() {

    lateinit var adapter: TabsFragmentAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        adapter = TabsFragmentAdapter(childFragmentManager)
        adapter.addFragment(PostListFragment.newInstance(false), getString(R.string.tab_all))
        adapter.addFragment(PostListFragment.newInstance(true), getString(R.string.tab_favorites))
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_tabs, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

    }

    override fun onResume() {
        super.onResume()
        view_pager.adapter = adapter
        tabs.setupWithViewPager(view_pager)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
    }

    companion object {

        fun newInstance() =
            TabsFragment()
    }
}
