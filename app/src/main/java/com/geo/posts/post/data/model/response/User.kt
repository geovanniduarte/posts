package com.geo.posts.post.data.model.response

class User (
    val id: Long,
    val name: String,
    val email: String,
    val phone: String,
    val website: String
)