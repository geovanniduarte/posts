package com.geo.posts.post.ui

import androidx.lifecycle.MutableLiveData
import com.geo.posts.base.vm.BaseViewModel
import com.geo.posts.post.data.model.entity.CommentEntity
import com.geo.posts.post.data.model.entity.PostEntity
import com.geo.posts.post.data.model.entity.UserEntity
import com.geo.posts.post.usecase.*
import io.reactivex.Scheduler
import javax.inject.Inject
import javax.inject.Named

class PostListViewModel @Inject constructor(private val getPostList: GetAllPostList,
                                            private val getFavoritePostList: GetFavoritePostList,
                                            private val updatePostTouched: UpdatePostTouched,
                                            private val markAsFavorite: MarkAsFavorite,
                                            private val getPostComments: GetPostComments,
                                            private val deletePosts: DeletePosts,
                                            private val getPostUser: GetPostUser,
                                            @Named("mainScheduler") private val mainScheduler: Scheduler): BaseViewModel() {

    val postAllListState: MutableLiveData<List<PostEntity>> = MutableLiveData()
    val postFavListState: MutableLiveData<List<PostEntity>> = MutableLiveData()
    val commentListState: MutableLiveData<List<CommentEntity>> = MutableLiveData()
    val postDeletedState: MutableLiveData<PostEntity> = MutableLiveData()
    val allPostDeleteState: MutableLiveData<Boolean> = MutableLiveData()
    val postUserState: MutableLiveData<UserEntity> = MutableLiveData()
    val showHomeButtonState : MutableLiveData<Boolean> = MutableLiveData()

    fun loadAllPosts(isReload: Boolean) {
        isLoadingState.value = true
        getPostList.execute(isReload, compositeDisposable,  onSuccess = {
            postAllListState.value = it
            postFavListState.value = emptyList()
            isLoadingState.value = false
        },
        onError = {
            handleApiError(it)
            isLoadingState.value = false
        })
    }

    fun loadFavPosts(isReload: Boolean) {
        isLoadingState.value = true
        getFavoritePostList.execute(isReload, compositeDisposable,  onSuccess = {
            postFavListState.value = it
            isLoadingState.value = false
        },
        onError = {
            handleApiError(it)
            isLoadingState.value = false
        })
    }

    fun loadComments(postId: Long) {
        isLoadingState.value = true
        getPostComments.execute(postId, compositeDisposable,  onSuccess = {
            commentListState.value = it
            isLoadingState.value = false
        },
        onError = {
            handleApiError(it)
            isLoadingState.value = false
        })
    }

    fun updatePostTouched(postEntity: PostEntity) {
        updatePostTouched.execute(postEntity)
    }

    fun markAsFavorite(favorite: Boolean, postEntity: PostEntity) {
        markAsFavorite.execute(favorite, postEntity)
    }

    fun deletePost(post: PostEntity) {
        deletePosts.execute(listOf(post), {}, {})
        postDeletedState.value = post
    }

    fun deleteAllPost() {
        deletePosts.execute(onComplete = {
            allPostDeleteState.value = true
        }, onError = {
            allPostDeleteState.value = false
        })
    }

    fun getPostUser(userId: Long) {
        getPostUser.execute(userId, compositeDisposable,
        onSuccess = {
            postUserState.value = it
        },
        onError = {
            handleApiError(it)
        })
    }

    fun showHomeButton(show: Boolean) {
        showHomeButtonState.value = show
    }
}