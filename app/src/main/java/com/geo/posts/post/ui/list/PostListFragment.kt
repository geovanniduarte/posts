package com.geo.posts.post.ui.list

import android.os.Bundle
import android.view.*
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import com.geo.posts.R
import com.geo.posts.base.Navigator
import com.geo.posts.base.ui.BaseFragment
import com.geo.posts.post.data.model.entity.PostEntity
import com.geo.posts.post.di.compoment.ListComponent
import com.geo.posts.post.ui.PostListViewModel
import kotlinx.android.synthetic.main.fragment_post_list.*
import javax.inject.Inject


private const val ARG_FAV = "is_favorite"

class PostListFragment : BaseFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var navigator: Navigator

    lateinit var viewModel: PostListViewModel

    private var isFavorite: Boolean = false

    lateinit var postLoader: PostLoader

    private val adapter =
        PostListAdapter ({ post, view ->
            onPostClicked(post, view)
        }) {
            onPostDeleted(it)
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        inject()
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_list, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_reload -> {
                resetView()
                postLoader.loadPosts(true)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onResume() {
        super.onResume()
    }

    private fun inject() {
        activityComponent?.inject(this)
    }

    override fun setUp(view: View?) {
        setUpViewModel()
        setUpPostList()
        setUpView()
        bindEvents()
        postLoader.loadPosts(false)
        viewModel.showHomeButton(false)
    }

    private fun setUpViewModel() {
        viewModel = ViewModelProvider(this.activity!!, viewModelFactory).get(PostListViewModel::class.java)
        postLoader = if (isFavorite) FavoritePostLoader(viewModel) else AllPostLoader(viewModel)
    }

    private fun setUpPostList() {
        post_list.layoutManager = LinearLayoutManager(this.context, LinearLayoutManager.VERTICAL, false)
        post_list.itemAnimator = DefaultItemAnimator()
        post_list.adapter = adapter
        val itemTouchHelper = ItemTouchHelper(SwipeToDeleteCallback(adapter, context!!))
        itemTouchHelper.attachToRecyclerView(post_list)
    }

    private fun setUpView() {

    }

    private fun resetView() {
        message.text = ""
    }

    private fun bindEvents() {
        postLoader.observePosts(this) {
            populatePostList(it)
        }

        viewModel.allPostDeleteState.observe(this, Observer {
            adapter.clear()
        })

        viewModel.errorMessageState.observe(this, Observer { message ->
            onError(message)
        })

        viewModel.postDeletedState.observe(this, Observer {
            removePostList(it)
        })
    }

    override fun onError(message: String?) {
        adapter.swapData(listOf())
        showMessage(message)
    }

    override fun showMessage(messageStr: String?) {
        message.text = messageStr
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_post_list, container, false)
        val component: ListComponent? = activityComponent
        component?.let {
            component.inject(this)
        }
        arguments?.let {
            isFavorite = it.getBoolean(ARG_FAV)
        }
        return view
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    override fun onDetach() {
        super.onDetach()
    }

    private fun populatePostList(posts: List<PostEntity>) {
        resetView()
        adapter.swapData(posts)
        if (posts.isEmpty()) {
            showMessage(getString(R.string.error_empty_list))
        }
    }

    private fun removePostList(post: PostEntity) {
        adapter.deleteLocal(post)
    }

    private fun onPostClicked(postEntity: PostEntity, view: View) {
        navigator.navigatesToDetail(this.activity!!.supportFragmentManager, postEntity, view)
        viewModel.showHomeButton(true)
    }

    private fun onPostDeleted(postEntity: PostEntity) {
        viewModel.deletePost(postEntity)
    }

    companion object {
        @JvmStatic
        fun newInstance(isFavorite: Boolean) =
            PostListFragment().apply {
                arguments = Bundle().apply {
                    putBoolean(ARG_FAV, isFavorite)
                }
            }
    }
}