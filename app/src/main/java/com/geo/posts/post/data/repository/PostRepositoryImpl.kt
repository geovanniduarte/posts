package com.geo.posts.post.data.repository

import com.geo.posts.post.data.model.entity.CommentEntity
import com.geo.posts.post.data.model.entity.PostEntity
import com.geo.posts.post.data.model.entity.UserEntity
import com.geo.posts.post.data.repository.datasource.PostApiDataSource
import com.geo.posts.post.data.repository.datasource.PostDataSource
import com.geo.posts.post.data.repository.datasource.PostLocalDataSource
import io.reactivex.Completable
import io.reactivex.Flowable

class PostRepositoryImpl(private val apiDataSource: PostApiDataSource, private val localDataSource: PostLocalDataSource): PostRepository {
    override fun getPostList(isReload: Boolean): Flowable<List<PostEntity>> {
        return if (isReload) {
            getPostsFromApi().doOnNext { localDataSource.savePosts(it) }
        } else {
            getPostsFromDb().concatWith(getPostsFromApi())
        }
    }

    private fun getPostsFromApi(): Flowable<List<PostEntity>> =
        apiDataSource.getAllPosts()


    private fun getPostsFromDb(): Flowable<List<PostEntity>> = localDataSource.getAllPosts()

    override fun saveUpdatePost(postEntity: PostEntity) {
        localDataSource.savePosts(listOf(postEntity))
    }

    override fun getPostComments(postId: Long): Flowable<List<CommentEntity>> =
        apiDataSource.getPostComments(postId)

    override fun deletePosts(posts: List<PostEntity>) {
        localDataSource.deletePosts(posts)
    }

    override fun deleteAllPosts(): Completable = localDataSource.deleteAllPosts()

    override fun getPostUser(userId: Long): Flowable<UserEntity> = apiDataSource.getPostUser(userId)

}