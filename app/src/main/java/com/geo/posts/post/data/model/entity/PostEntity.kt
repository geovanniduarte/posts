package com.geo.posts.post.data.model.entity

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Entity(tableName="posts")
@Parcelize
data class PostEntity(
    val userId: Long,
    @PrimaryKey()
    val id: Long,
    val title: String,
    val body: String,
    var favorite: Boolean = false,
    var touched: Boolean = false
): Parcelable