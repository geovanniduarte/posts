package com.geo.posts.post.usecase

import com.geo.posts.base.rx.mapNetworkErrors
import com.geo.posts.post.data.model.entity.CommentEntity
import com.geo.posts.post.data.model.entity.PostEntity
import com.geo.posts.post.data.repository.PostRepositoryImpl
import io.reactivex.Flowable
import io.reactivex.Scheduler
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import javax.inject.Inject
import javax.inject.Named

class GetPostComments @Inject constructor(private val postRepositoryImpl: PostRepositoryImpl,
                                         @Named("ioScheduler") private val ioScheduler: Scheduler,
                                         @Named("mainScheduler") private val mainScheduler: Scheduler
) {

    fun buildCase(postId: Long): Flowable<List<CommentEntity>> =
        postRepositoryImpl.getPostComments(postId)
            .mapNetworkErrors()

    fun execute(postId: Long, compositeDisposable: CompositeDisposable, onSuccess: (value: List<CommentEntity>) -> Unit, onError: (t: Throwable) -> Unit = {}) {
        buildCase(postId)
            .subscribeOn(ioScheduler)
            .observeOn(mainScheduler)
            .subscribeBy(onNext = onSuccess, onError = onError)
            .addTo(compositeDisposable)
    }
}