package com.geo.posts.post.usecase

import com.geo.posts.post.data.model.entity.PostEntity
import com.geo.posts.post.data.repository.PostRepositoryImpl
import javax.inject.Inject

class MarkAsFavorite  @Inject constructor(private val postRepositoryImpl: PostRepositoryImpl) {
    fun execute(isFavorite: Boolean, postEntity: PostEntity) {
        postEntity.favorite = isFavorite
        postRepositoryImpl.saveUpdatePost(postEntity)
    }
}