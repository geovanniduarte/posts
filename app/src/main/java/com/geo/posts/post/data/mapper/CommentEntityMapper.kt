package com.geo.posts.post.data.mapper

import com.geo.posts.base.mapper.Mapper
import com.geo.posts.post.data.model.entity.CommentEntity
import com.geo.posts.post.data.model.response.Comment

class CommentEntityMapper: Mapper<Comment, CommentEntity> {

    override fun transform(input: Comment): CommentEntity =
        CommentEntity(input.postId, input.id, input.name, input.email, input.body)

    override fun transformList(inputList: List<Comment>): List<CommentEntity> =
        inputList.map { transform(it) }
}