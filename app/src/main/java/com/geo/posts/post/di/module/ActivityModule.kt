package com.geo.posts.post.di.module

import android.app.Activity
import androidx.appcompat.app.AppCompatActivity
import com.geo.posts.base.di.ActivityContext
import com.geo.posts.base.di.PerActivity
import com.geo.posts.post.data.repository.PostRepositoryImpl
import com.geo.posts.post.usecase.GetAllPostList
import com.geo.posts.post.usecase.GetPostUser
import com.geo.posts.post.usecase.UpdatePostTouched
import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Named

@Module
class ActivityModule(private val activity: AppCompatActivity) {

    @Provides
    @ActivityContext
    @PerActivity
    fun provideActivityContext(): Activity = activity

    @Provides
    @PerActivity
    fun provideCompositeDisposable(): CompositeDisposable = CompositeDisposable()

    @Provides
    @PerActivity
    fun provideGetPostListUseCase(postRepositoryImpl: PostRepositoryImpl, @Named("ioScheduler") ioScheduler: Scheduler, @Named("mainScheduler") mainScheduler: Scheduler) : GetAllPostList
            = GetAllPostList(postRepositoryImpl, ioScheduler, mainScheduler)

    @Provides
    @PerActivity
    fun provideUpdatePostUseCase(postRepositoryImpl: PostRepositoryImpl): UpdatePostTouched = UpdatePostTouched(postRepositoryImpl)

    @Provides
    @PerActivity
    fun provideGetPostUserUseCase(postRepositoryImpl: PostRepositoryImpl, @Named("ioScheduler") ioScheduler: Scheduler, @Named("mainScheduler") mainScheduler: Scheduler) : GetPostUser
            = GetPostUser(postRepositoryImpl, ioScheduler, mainScheduler)
}