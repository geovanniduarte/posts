package com.geo.posts.post.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.geo.posts.post.data.model.entity.PostEntity

@Database(entities = [PostEntity::class], version = 1)
abstract class PostDatabase: RoomDatabase() {
    abstract fun getPostDao(): PostDao
}