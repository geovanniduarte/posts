package com.geo.posts.post.data.repository.datasource

import com.geo.posts.post.data.db.PostDatabase
import com.geo.posts.post.data.model.entity.CommentEntity
import com.geo.posts.post.data.model.entity.PostEntity
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers

class PostLocalDataSource(private val postDatabase: PostDatabase): PostDataSource, LocalDataSource {

    override fun savePosts(posts: List<PostEntity>) {
        Observable.fromCallable {
            postDatabase.getPostDao().insertAll(posts)
        }
        .subscribeOn(Schedulers.io())
        .subscribe()
    }

    override fun getAllPosts(): Flowable<List<PostEntity>> =
        postDatabase.getPostDao().getAllPosts().toFlowable()

    override fun getPostComments(postId: Long): Flowable<List<CommentEntity>> = Flowable.empty()

    override fun deletePosts(posts: List<PostEntity>) {
        Observable.fromCallable {
            postDatabase.getPostDao().deletePosts(posts)
        }
        .subscribeOn(Schedulers.io())
        .subscribe()
    }

    override fun deleteAllPosts(): Completable = postDatabase.getPostDao().deleteAllPosts()
}