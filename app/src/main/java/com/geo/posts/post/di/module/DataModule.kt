package com.geo.posts.post.di.module

import android.content.Context
import androidx.room.Room
import com.geo.posts.base.Constants
import com.geo.posts.post.data.db.PostDatabase
import com.geo.posts.post.data.mapper.CommentEntityMapper
import com.geo.posts.post.data.mapper.PostEntityMapper
import com.geo.posts.post.data.mapper.UserEntityMapper
import com.geo.posts.post.data.net.PostService
import com.geo.posts.post.data.repository.datasource.PostApiDataSource
import com.geo.posts.post.data.repository.datasource.PostDataSource
import com.geo.posts.post.data.repository.datasource.PostLocalDataSource
import com.geo.posts.post.data.repository.PostRepositoryImpl
import dagger.Module
import dagger.Provides
import javax.inject.Named
import javax.inject.Singleton

@Module
class DataModule {

    @Singleton
    @Provides
    fun providePostEntityMapper(): PostEntityMapper = PostEntityMapper()

    @Singleton
    @Provides
    fun provideCommentEntityMapper(): CommentEntityMapper = CommentEntityMapper()

    @Singleton
    @Provides
    fun provideUserEntityMapper(): UserEntityMapper = UserEntityMapper()

    @Singleton
    @Provides
    fun provideApiDataSource(productService: PostService, productEntityMapper: PostEntityMapper,
                             commentEntityMapper: CommentEntityMapper, userEntityMapper: UserEntityMapper) : PostApiDataSource =
        PostApiDataSource(productService, productEntityMapper, commentEntityMapper, userEntityMapper)

    @Singleton
    @Provides
    fun provideDatabase(context: Context): PostDatabase = Room.databaseBuilder(context, PostDatabase::class.java, Constants.POST_DATABASE_NAME).build()

    @Singleton
    @Provides
    fun provideLocalDataSource(postDatabase: PostDatabase) : PostLocalDataSource =
        PostLocalDataSource(postDatabase)

    @Singleton
    @Provides
    fun providePostRepository(apiDataSource: PostApiDataSource, localDataSource: PostLocalDataSource): PostRepositoryImpl =
        PostRepositoryImpl(
            apiDataSource,
            localDataSource
        )
}