package com.geo.posts.post.di.compoment

import android.content.Context
import com.geo.posts.base.Navigator
import com.geo.posts.post.data.repository.PostRepositoryImpl
import com.geo.posts.post.di.module.ApplicationModule
import com.geo.posts.post.di.module.DataModule
import com.geo.posts.post.di.module.NetModule
import dagger.Component
import io.reactivex.Scheduler
import javax.inject.Named
import javax.inject.Singleton

@Singleton
@Component(modules = [ApplicationModule::class, NetModule::class, DataModule::class])
interface ApplicationComponent {
    fun getContext(): Context
    fun getPostListRepository(): PostRepositoryImpl
    fun getNavigator(): Navigator
    @Named("mainScheduler")
    fun getMainScheduler(): Scheduler
    @Named("ioScheduler")
    fun getIOScheduler(): Scheduler
}