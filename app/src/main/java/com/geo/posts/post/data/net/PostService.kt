package com.geo.posts.post.data.net

import com.geo.posts.post.data.model.response.Comment
import com.geo.posts.post.data.model.response.Post
import com.geo.posts.post.data.model.response.User
import io.reactivex.Flowable
import retrofit2.http.GET
import retrofit2.http.Path

interface PostService {
    @GET(value = "posts")
    fun getPosts(): Flowable<List<Post>>

    @GET(value = "posts/{id}/comments")
    fun getPostsComments(@Path("id") postId: Long): Flowable<List<Comment>>

    @GET(value = "users/{id}")
    fun getPostUser(@Path("id") userId: Long): Flowable<User>
}