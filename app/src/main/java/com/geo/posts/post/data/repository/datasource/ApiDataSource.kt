package com.geo.posts.post.data.repository.datasource

import com.geo.posts.post.data.model.entity.UserEntity
import io.reactivex.Flowable

interface ApiDataSource {
    fun getPostUser(userId: Long): Flowable<UserEntity>
}