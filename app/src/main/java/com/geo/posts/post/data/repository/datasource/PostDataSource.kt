package com.geo.posts.post.data.repository.datasource

import com.geo.posts.post.data.model.entity.CommentEntity
import com.geo.posts.post.data.model.entity.PostEntity
import io.reactivex.Flowable

interface PostDataSource {
    fun getAllPosts(): Flowable<List<PostEntity>>
    fun getPostComments(postId: Long): Flowable<List<CommentEntity>>
}