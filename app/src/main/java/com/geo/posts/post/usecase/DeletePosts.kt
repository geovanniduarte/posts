package com.geo.posts.post.usecase

import com.geo.posts.post.data.model.entity.PostEntity
import com.geo.posts.post.data.repository.PostRepositoryImpl
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class DeletePosts@Inject constructor(private val postRepositoryImpl: PostRepositoryImpl) {
    fun execute(posts: List<PostEntity> = emptyList(),  onComplete: () -> Unit, onError: (t: Throwable) -> Unit = {}) {
        if (posts.isNotEmpty()) {
            postRepositoryImpl.deletePosts(posts)
        } else {
            postRepositoryImpl.deleteAllPosts()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(onComplete, onError)
        }
    }
}