package com.geo.posts.post.data.repository

import com.geo.posts.post.data.model.entity.CommentEntity
import com.geo.posts.post.data.model.entity.PostEntity
import com.geo.posts.post.data.model.entity.UserEntity
import io.reactivex.Completable
import io.reactivex.Flowable

interface PostRepository {
    fun saveUpdatePost(postEntity: PostEntity)
    fun getPostList(isReload: Boolean): Flowable<List<PostEntity>>
    fun getPostComments(postId: Long): Flowable<List<CommentEntity>>
    fun deletePosts(posts: List<PostEntity>)
    fun deleteAllPosts(): Completable
    fun getPostUser(userId: Long): Flowable<UserEntity>
}