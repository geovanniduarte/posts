package com.geo.posts.post.data.db

import androidx.room.*
import com.geo.posts.post.data.model.entity.PostEntity
import io.reactivex.Completable
import io.reactivex.Maybe

@Dao
abstract class PostDao {
    
    @Query("SELECT * FROM posts")
    abstract fun getAllPosts(): Maybe<List<PostEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertAll(posts: List<PostEntity>)

    @Query("DELETE FROM posts")
    abstract fun deleteAllPosts(): Completable

    @Delete
    @Transaction
    abstract fun deletePosts(posts: List<PostEntity>)

    @Transaction
    open fun removeAndInsertPosts(posts: List<PostEntity>) {
        deleteAllPosts()
        insertAll(posts)
    }
}