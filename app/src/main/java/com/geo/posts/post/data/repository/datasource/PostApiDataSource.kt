package com.geo.posts.post.data.repository.datasource

import com.geo.posts.post.data.mapper.CommentEntityMapper
import com.geo.posts.post.data.mapper.PostEntityMapper
import com.geo.posts.post.data.mapper.UserEntityMapper
import com.geo.posts.post.data.model.entity.CommentEntity
import com.geo.posts.post.data.model.entity.PostEntity
import com.geo.posts.post.data.model.entity.UserEntity
import com.geo.posts.post.data.net.PostService
import io.reactivex.Flowable

class PostApiDataSource(private val postService: PostService, private val postEntityMapper: PostEntityMapper,
                        private val commentEntityMapper: CommentEntityMapper, private val userEntityMapper: UserEntityMapper)
    :PostDataSource, ApiDataSource {

    override fun getAllPosts(): Flowable<List<PostEntity>> =
        postService.getPosts().map { postEntityMapper.transformList(it) }

    override fun getPostComments(postId: Long): Flowable<List<CommentEntity>> =
        postService.getPostsComments(postId).map {  commentEntityMapper.transformList(it) }

    override fun getPostUser(userId: Long): Flowable<UserEntity> = postService.getPostUser(userId)
        .map { userEntityMapper.transform(it) }

}