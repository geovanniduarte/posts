package com.geo.posts.post.ui.detail

import android.os.Bundle
import android.view.*
import androidx.core.content.ContextCompat
import androidx.core.view.ViewCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.geo.comments.comment.ui.detail.CommentListAdapter

import com.geo.posts.R
import com.geo.posts.base.Constants.Companion.VIEW_DETAIL_TRANSITION_NAME
import com.geo.posts.base.ui.BaseFragment
import com.geo.posts.post.data.model.entity.CommentEntity
import com.geo.posts.post.data.model.entity.PostEntity
import com.geo.posts.post.data.model.entity.UserEntity
import com.geo.posts.post.ui.PostListViewModel
import kotlinx.android.synthetic.main.activity_post_list.*
import kotlinx.android.synthetic.main.fragment_post_detail.*
import kotlinx.android.synthetic.main.fragment_post_list.*
import javax.inject.Inject

private const val ARG_POST = "post"

class PostDetailFragment : BaseFragment() {

    companion object {
        fun newInstance(post: PostEntity) = PostDetailFragment().apply {
            arguments = Bundle().apply {
                putParcelable(ARG_POST, post)
            }
        }
    }

    private var post: PostEntity? = null

    private var isFavorite: Boolean = false

    lateinit var viewModel: PostListViewModel

    private val adapter = CommentListAdapter()

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        inject()
        val view =  inflater.inflate(R.layout.fragment_post_detail, container, false)
        arguments?.let {
            post = it.getParcelable(ARG_POST)
        }
        return view
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_detail, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_favorite -> {
                toggleFavoriteItem(item)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        super.onPrepareOptionsMenu(menu)

        val favMenu = menu?.findItem(R.id.action_favorite)
        setFavoriteIcon(favMenu)

    }

    private fun setFavoriteIcon(item: MenuItem) {
        item.icon = if (isFavorite) ContextCompat.getDrawable(context!!, R.drawable.ic_star) else ContextCompat.getDrawable(context!!, R.drawable.ic_empty_star)
    }

    private fun toggleFavoriteItem(item: MenuItem) {
        isFavorite = !isFavorite
        post?.let {
            viewModel.markAsFavorite(isFavorite, it)
        }
        setFavoriteIcon(item)
    }

    private fun inject() {
        activityComponent?.inject(this)
    }

    override fun setUp(view: View?) {
        ViewCompat.setTransitionName(detail_description_body_value, VIEW_DETAIL_TRANSITION_NAME)
        setUpViewModel()
        setUpCommentList()
        bindEvents()
        hideKeyboard()
        post?.let {
            isFavorite = it.favorite
            syncView(it)
            viewModel.updatePostTouched(it)
            viewModel.loadComments(it.id)
            viewModel.getPostUser(it.userId)
        }
    }

    private fun setUpViewModel() {
        viewModel = ViewModelProvider(this, viewModelFactory).get(PostListViewModel::class.java)
    }

    private fun setUpCommentList() {
        comments_list.layoutManager = LinearLayoutManager(this.context, LinearLayoutManager.VERTICAL, false)
        comments_list.itemAnimator = DefaultItemAnimator()
        comments_list.adapter = adapter
    }

    private fun syncView(postEntity: PostEntity) {
        detail_description_body_value.text = postEntity.body
    }

    private fun syncUser(it: UserEntity) {
        txv_detail_user_email.text = it.email
        txv_detail_user_name.text = it.name
        txv_detail_user_phone.text = it.phone
        txv_detail_user_site.text = it.website
    }

    private fun bindEvents() {
        viewModel.commentListState.observe(this, Observer {
            populateCommentList(it)
        })

        viewModel.postUserState.observe(this, Observer {
            syncUser(it)
        })
    }

    private fun populateCommentList(posts: List<CommentEntity>) {
        adapter.swapData(posts)
    }

}