package com.geo.posts.post.data.model.entity

class UserEntity(
    val id: Long,
    val name: String,
    val email: String,
    val phone: String,
    val website: String
)