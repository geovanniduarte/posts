package com.geo.posts.post.di.compoment

import com.geo.posts.base.di.PerActivity
import com.geo.posts.post.di.module.ActivityModule
import com.geo.posts.post.di.module.VMModule
import com.geo.posts.post.ui.detail.PostDetailFragment
import com.geo.posts.post.ui.list.ListActivity
import com.geo.posts.post.ui.list.PostListFragment
import dagger.Component

@PerActivity
@Component(modules = [ActivityModule::class, VMModule::class], dependencies = [ApplicationComponent::class])
interface ListComponent {
    fun inject(activity: ListActivity)
    fun inject(listFragment: PostListFragment)
    fun inject(detailFragment: PostDetailFragment)
}